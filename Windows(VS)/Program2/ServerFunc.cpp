#include "ServerFunc.h"
#include <iostream>

int InitServer(struct sockaddr_in& local,/*int &mySocket; UNIX*/ SOCKET& mySocket /*Windows*/,/*int &clientSocket; UNIX*/ SOCKET& clientSocket /*Windows*/)
{
	//WSAStartup /*Windows*/
	WSAData wsaData;
	WORD DLLVersion = MAKEWORD(2, 1);
	if (WSAStartup(DLLVersion, &wsaData) != 0)
	{
		std::cout << "Error\n";
		exit(1);
	}

	int rc;
	local.sin_family = AF_INET;
	local.sin_port = htons(7500);
	local.sin_addr.s_addr = htonl(INADDR_ANY);

	mySocket = socket(AF_INET, SOCK_STREAM, 0);
	if (/*s < 0 UNIX*/ mySocket == INVALID_SOCKET /*Windows*/)
	{
		std::cout << "Socket call error\n";
		exit(1);
	}

	rc = bind(mySocket, (struct sockaddr*)&local, sizeof(local));
	if (/*rc < 0 UNIX */rc == SOCKET_ERROR /*Windows*/)
	{
		std::cout << "Bind call error\n";
		return rc;
	}

	rc = listen(mySocket, 5);
	if (/*rc UNIX */rc == SOCKET_ERROR /*Windows*/)
	{
		std::cout << "Error calling \"listen\"\n";
		return rc;
	}
	return rc;
}
