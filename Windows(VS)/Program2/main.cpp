
#include "ServerFunc.h"

#include <iostream>
#include <string>

int main(void)
{

	struct sockaddr_in local;
	/*int s; UNIX*/ SOCKET mySocket;
	/*int s1; UNIX*/ SOCKET clientSocket;
	

	if (InitServer(local, mySocket, clientSocket) == SOCKET_ERROR)
	{
		exit(1);
	}

	std::string buf = "000";

	bool bExitProg = false;
	char ch;
	while (!bExitProg)
	{
		std::cout << "Waiting for client connection...\n";
		clientSocket = accept(mySocket, NULL, NULL);
		if (/*clientSocket < 0 UNIX*/ clientSocket == INVALID_SOCKET /*Windows*/)
		{
			std::cout << "Client connection error.\n";
			std::cout << "Select and press Enter:\n";
			std::cout << "\tAny key - try again.\n";
			std::cout << "\te - exit the program.\n";

			ch = getchar();
			getchar();
			if (ch == 101)
			{
				bExitProg = true;
			}
		}
		else 
		{
			std::cout << "Client connected.\n";

			bool bTryAgain = true;
			while (bTryAgain)
			{
				std::cout << "\nWaiting for data...\n";
				int rc;
				rc = recv(clientSocket, &(buf[0]), 3, 0);
				if (rc <= 0)
				{
					std::cout << "Error while getting data. Client connection lost.";
					std::cout << "Select and press Enter:\n";
					std::cout << "\tAny key - try to reconnect.\n";
					std::cout << "\te - exit the program.\n";
					ch = getchar();
					getchar();
					if (ch == 101)
					{
						bExitProg = true;
					}

					bTryAgain = false;
				}
				else
				{
					std::cout << "Data received:\n";
					if (rc > 1)
					{
						std::string str;
						str.assign(buf, 0, rc);
						buf = "000";
						if (std::stoi(str) % 32 == 0)
						{
							std::cout << str << std::endl;
						}
						else
						{
							std::cout << "received data is incorrect.\n";
						}
					}
					else
					{
						std::cout << "received data is incorrect\n";
					}

					std::cout << "\nSelect and press Enter:\n";
					std::cout << "\tAny key - get more data.\n";
					std::cout << "\te - exit the program.\n";
					ch = getchar();
					getchar();
					if (ch == 27)
					{
						bTryAgain = false;
						bExitProg = true;
					}
				}
			}
		}
	}
}
