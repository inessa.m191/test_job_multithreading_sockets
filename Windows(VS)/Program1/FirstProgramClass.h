#pragma once
#include "JoinServer.h"
#include <mutex>

class FirstProgramClass
{
	struct sockaddr_in peer;
	/*int s; UNIX*/ SOCKET s; /*Windows*/
	std::mutex mtxWorkWithBuffer;
	std::mutex mtxThread2Waiting;
	std::mutex mtxThread2WaitingConnection;
	std::string* buf = nullptr;
	bool bBufferEmpty = true;
	bool bThread2Waiting = false;
	bool bConnectionMade = false;
	bool bThread2WaitingConnection = false;

public:
	FirstProgramClass();

	~FirstProgramClass()
	{
		if (buf)
		{
			delete buf;
		}
	}

	void ProgMenu();

	/*int UNIX*/ SOCKET /*Windows*/ ClassInitClient()
	{
		return InitClient(peer, s);
	}

	bool ClassConnectServer()
	{
		bConnectionMade = ConnectServer(peer, s, sizeof(peer));;
		return bConnectionMade;
	}

	int AddDataToBuffer();

	void SecondThread();

	void SendToSecondProgram(int package);
};

