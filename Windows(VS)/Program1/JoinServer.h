#pragma once
#pragma comment(lib, "ws2_32.lib") /*Windows*/
#include <Ws2tcpip.h> /*Windows*/
#pragma warning(disable:4996) /*Windows*/
#include <winsock2.h> /*Windows*/

//#include <sys/types.h> /*UNIX*/
//#include <sys/socket.h> /*UNIX*/
//#include <netinet/in.h> /*UNIX*/
//#include <arpa/inet.h> /*UNIX*/
#include <stdio.h>

/*int UNIX*/ SOCKET /*Windows*/ InitClient(struct sockaddr_in& peer,/*int &s; UNIX*/ SOCKET& s /*Windows*/);

int ConnectServer(struct sockaddr_in& peer,/*int &s; UNIX*/ SOCKET& s /*Windows*/, int sizePeer);