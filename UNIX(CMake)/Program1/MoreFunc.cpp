#include "MoreFunc.h"
#include <iostream>
#include <algorithm> 

void StringInput(std::string& str)
{
	std::cout << "\nEnter a numeric-only string up to 64 characters long.\nPress \"Enter\" to finish entering.\n";
	char ch;
	int i = 0;
	int j = 0;
	std::string teststr;
	std::getline(std::cin, teststr);

	while (j < teststr.length() && i < 64)
	{
		ch = teststr[j];
		if (ch > 47 && ch < 58)
		{
			str += ch;
			std::cout << str[i];
			i++;
		}
		j++;
	}
	std::cout << std::endl;
}