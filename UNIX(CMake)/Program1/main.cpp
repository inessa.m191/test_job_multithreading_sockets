#include "FirstProgramClass.h"
#include <iostream>
#include <thread>
#include "MoreFunc.h"


int main(void)
{
	FirstProgramClass* start = new FirstProgramClass;

	start->ClassConnectServer();

	std::thread th(&FirstProgramClass::SecondThread, start);

	start->ProgMenu();

	exit(0);
}