#include "JoinServer.h"
#include <iostream>

int InitClient(struct sockaddr_in& peer, int &s)
{
	peer.sin_family = AF_INET;
	peer.sin_port = htons(7500);
	peer.sin_addr.s_addr = inet_addr("127.0.0.1");

	s = socket(AF_INET, SOCK_STREAM, 0);

	return s;
}

int ConnectServer(struct sockaddr_in& peer, int& s, int sizePeer)
{
	std::cout << "| Server connection...         |                              |\n";
	int rc = 1;
	rc = connect(s, (struct sockaddr*)&peer, sizePeer);
	while (rc)
	{
		std::cout << "| Connection error.            |\n";
		std::cout << "| Select and press Enter:      |\n";
		std::cout << "| Any key - reconnect.         |\n";
		std::cout << "| e - exit to main menu.       |\n";
		char x = (getchar());
		getchar();
		if (x == 101)
		{
			std::cout << "|                              |                              |\n";
			std::cout << "| Connection NOT established.  |                              |\n";
			std::cout << "|                              |                              |\n";
			return 0;
		}
		std::cout << "| Server connection...         |                              |\n";
		rc = connect(s, (struct sockaddr*)&peer, sizePeer);
	}
	std::cout << "|                              |                              |\n";
	std::cout << "| Connection established.      |                              |\n";
	std::cout << "|                              |                              |\n";
	return 1;
}
