#pragma once

#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <stdio.h>

int InitClient(struct sockaddr_in& peer, int& s);

int ConnectServer(struct sockaddr_in& peer, int& s, int sizePeer);
