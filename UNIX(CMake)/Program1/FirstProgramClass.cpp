#include "FirstProgramClass.h"
#include <iostream>
#include <string>
#include <algorithm>
#include <thread>
#include "MoreFunc.h"


FirstProgramClass::FirstProgramClass()
{
	mtxThread2Waiting.lock();
	mtxThread2WaitingConnection.lock();

	if (ClassInitClient() < 0)
	{
		std::cout << "Socket call error\n";
		exit(1);
	}
}

void FirstProgramClass::ProgMenu()
{
	bool bExitProg = false;
	char ch;
	while (!bExitProg)
	{
		std::cout << "| Thread 1:                    | Thread 2:                    |\n";
		std::cout << "| Select and press Enter:      |                              |\n";
		std::cout << "| 1 - Fill buffer.             |                              |\n";
		std::cout << "| 2 - Establish a connection.  |                              |\n";
		std::cout << "| e - Exiting the program.     |                              |\n";
		std::cout << "|                              |                              |\n";
		ch = getchar();
		getchar();
		switch (ch)
		{
		case 49:
			if (bBufferEmpty)
			{
				mtxWorkWithBuffer.lock();
				bBufferEmpty = false;
				AddDataToBuffer();
				mtxWorkWithBuffer.unlock();

				if (bThread2Waiting)
				{
					mtxThread2Waiting.unlock();
					std::this_thread::sleep_for(std::chrono::milliseconds(5));
					mtxThread2Waiting.lock();
				}
			}
			else
			{
				std::cout << "| Buffer is full.              |                              |\n";
				std::cout << "|                              |                              |\n";
			}

			break;

		case 50:
			if (!bConnectionMade)
			{
				if (ClassInitClient() < 0)
				{
					std::cout << "Socket call error\n";
					exit(1);
				}

				if (ClassConnectServer())
				{
					if (bThread2WaitingConnection)
					{
						mtxThread2WaitingConnection.unlock();
						std::this_thread::sleep_for(std::chrono::milliseconds(5));
						mtxThread2WaitingConnection.lock();
					}
				}
			}
			else
			{
				std::cout << "|                              |                              |\n";
				std::cout << "| Connection established.      |                              |\n";
				std::cout << "|                              |                              |\n";
			}

			break;

		case 101: bExitProg = true;
			break;

		default: continue;
			break;
		}
	}
}

int FirstProgramClass::AddDataToBuffer()
{
	std::string str;
	StringInput(str);
	std::sort(str.begin(), str.end(), [](char a, char b){return a > b;});

	std::string replaceStr = "KB";
	char ch;

	for (unsigned int i = 0; i < str.length(); ++i)
	{
		ch = str[i];
		if ((ch-'0') % 2 == 0)
		{
			str.replace(i, 1, replaceStr);
			++i;
		}
	}

	buf = new std::string;
	buf->assign(str);

	return 0;
}

void FirstProgramClass::SecondThread()
{
	std::string str;
	bool bDataGot = false;
	bool bFirstTry = true;

	while (true)
	{
		{
			bDataGot = false;
			bFirstTry = true;
			while (!bDataGot)
			{
				if (!bBufferEmpty)
				{
					mtxWorkWithBuffer.lock();
					str.assign(*buf);
					delete buf;
					buf = nullptr;
					bBufferEmpty = true;
					mtxWorkWithBuffer.unlock();
					bDataGot = true;
				}
				else
				{
					bThread2Waiting = true;
					if (bFirstTry)
					{
						std::cout << "|                              |                              |\n";
						std::cout << "|                              | Waiting for data...          |\n";
						std::cout << "|                              |                              |\n";
						bFirstTry = false;
					}
					mtxThread2Waiting.lock();
					bThread2Waiting = false;
					mtxThread2Waiting.unlock();
				}
			}
		}

		std::cout << "|                              |                              |\n";
		std::cout << "|                              | Data received from the buffer:\n";
		std::cout << "|                              | " << str << std::endl;

		int result = 0;
		for (const char ch : str)
		{
			if (ch > 47 && ch < 58)
			{
				result += atoi(&ch);
			}
		}

		SendToSecondProgram(result);

	}
}

void FirstProgramClass::SendToSecondProgram(int package)
{
	std::string str = std::to_string(package);
	int rc;
	char responsePacket;

	bool bDataSent = false;
	while (!bDataSent)
	{
		if (bConnectionMade)
		{
			send(s, &(str[0]), 3, 0);
			rc = recv(s, &responsePacket, 1, 0);
			if (rc <= 0)
			{
				bConnectionMade = false;
				std::cout << "|                              |                              |\n";
				std::cout << "|                              | Send error.                  |\n";
				std::cout << "|                              | Connection lost.             |\n";
				std::cout << "|                              |                              |\n";
			}
			else
			{
				bDataSent = true;
				std::cout << "|                              |                              |\n";
				std::cout << "|                              | Packet sent successfully.    |\n";
				std::cout << "|                              |                              |\n";
			}
		}
		else
		{
			bThread2WaitingConnection = true;
			std::cout << "|                              |                              |\n";
			std::cout << "|                              | There is no connection to    |\n";
			std::cout << "|                              | send data. Try to reconnect. |\n";
			std::cout << "|                              |                              |\n";

			mtxThread2WaitingConnection.lock();
			bThread2WaitingConnection = false;
			mtxThread2WaitingConnection.unlock();
		}
	}
}
