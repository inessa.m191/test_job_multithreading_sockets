
#include "ServerFunc.h"

#include <iostream>
#include <string>

int main(void)
{

	struct sockaddr_in local;
	int mySocket;
	int clientSocket;
	

	if (InitServer(local, mySocket, clientSocket) < 0)
	{
		exit(1);
	}

	std::string buf = "000";

	bool bExitProg = false;
	char ch;
	while (!bExitProg)
	{
		std::cout << "Waiting for client connection...\n";
		clientSocket = accept(mySocket, NULL, NULL);
		if (clientSocket < 0)
		{
			std::cout << "Client connection error.\n";
			std::cout << "Select and press Enter:\n";
			std::cout << "\tAny key - try again.\n";
			std::cout << "\te - exit the program.\n";

			ch = getchar();
			getchar();
			if (ch == 101)
			{
				bExitProg = true;
			}
		}
		else 
		{
			std::cout << "Client connected.\n";

			bool bTryAgain = true;
			while (bTryAgain)
			{
				std::cout << "\nWaiting for data...\n";
				int rc;
				rc = recv(clientSocket, &(buf[0]), 3, 0);
				if (rc <= 0)
				{
					std::cout << "Error while getting data. Client connection lost.";
					std::cout << "Select and press Enter:\n";
					std::cout << "\tAny key - try to reconnect.\n";
					std::cout << "\te - exit the program.\n";
					ch = getchar();
					getchar();
					if (ch == 101)
					{
						bExitProg = true;
					}

					bTryAgain = false;
				}
				else
				{
					send(clientSocket, "1", 1, 0);
					std::cout << "Data received:\n";
					if (rc > 1)
					{
						if (std::stoi(buf) % 32 == 0)
						{
							std::cout << buf << std::endl;
						}
						else
						{
							std::cout << "received data is incorrect.\n";
						}
					}
					else
					{
						std::cout << "received data is incorrect\n";
					}

					std::cout << "\nSelect and press Enter:\n";
					std::cout << "\tAny key - get more data.\n";
					std::cout << "\te - exit the program.\n";
					ch = getchar();
					getchar();
					if (ch == 101)
					{
						bTryAgain = false;
						bExitProg = true;
					}
				}
			}
		}
	}
}
