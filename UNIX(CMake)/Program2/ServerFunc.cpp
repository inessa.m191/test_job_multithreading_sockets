#include "ServerFunc.h"
#include <iostream>

int InitServer(struct sockaddr_in& local, int& mySocket, int& clientSocket)
{

	int rc;
	local.sin_family = AF_INET;
	local.sin_port = htons(7500);
	local.sin_addr.s_addr = htonl(INADDR_ANY);

	mySocket = socket(AF_INET, SOCK_STREAM, 0);
	if (mySocket < 0)
	{
		std::cout << "Socket call error\n";
		exit(1);
	}

	rc = bind(mySocket, (struct sockaddr*)&local, sizeof(local));
	if (rc < 0)
	{
		std::cout << "Bind call error\n";
		return rc;
	}

	rc = listen(mySocket, 5);
	if (rc < 0)
	{
		std::cout << "Error calling \"listen\"\n";
		return rc;
	}
	return rc;
}
