#pragma once


#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h> 
#include <stdio.h>

int InitServer(struct sockaddr_in& local,int &mySocket,int &clientSocket);
